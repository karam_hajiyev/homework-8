package az.ibatech;

public class Fish extends Pet {
    // constructors
    public Fish() {}
    public Fish(String nickname, int age, int trickLevel) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.species = Species.FISH;
    }
}

