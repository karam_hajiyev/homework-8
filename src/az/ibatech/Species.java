package az.ibatech;

public enum Species {
    DOG,
    DOMESTICCAT,
    ROBOCAT,
    FISH,
    UNKNOWN
}

