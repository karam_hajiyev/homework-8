package az.ibatech;

public class Dog extends Pet {
    // constructors
    public Dog() {
    }

    public Dog(String nickname, int age, int trickLevel) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.species = Species.DOG;
    }

    // methods
    public void foul() {
        System.out.println("I need to cover it up.");
    }
}


