package az.ibatech;

public final class Woman extends Human {
    // constructors
    public Woman() {
    }

    public Woman(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Woman(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
    }

    public void makeUp() {
        System.out.println("It's time to go to the beautician.");
    }

    public void greetPet() {
        System.out.println("Hello, my sweeties " + family.getPet().getNickname() + '.');
    }
}
