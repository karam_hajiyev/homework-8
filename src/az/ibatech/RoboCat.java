package az.ibatech;

public class RoboCat extends Pet {
    // constructors
    public RoboCat() {
    }

    public RoboCat(String nickname, int age, int trickLevel) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.species = Species.ROBOCAT;
    }
}
